﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
	Menu = 0,
	Level1,
	Level2,
	MenuPause

}
public class GlobalControll : MonoBehaviour
{
	public GameObject Level1;
	public GameObject Menu;
	public GameObject Level2;
	public GameObject MenuPause;
	public GameState CurrState = GameState.Menu;
	public GameState LastState = GameState.Menu;

	void Start()
	{
		ChangeGameState(CurrState);
	}

	public void ChangeGameState(GameState gameState)
	{
		LastState = CurrState;
		switch (gameState)
		{
			case GameState.Menu:
				Level1.SetActive(false);
				Level2.SetActive(false);
				MenuPause.SetActive(false);
				Menu.SetActive(true);
				break;

			case GameState.Level1:
				Level1.SetActive(true);
				Level2.SetActive(false);
				MenuPause.SetActive(false);
				Menu.SetActive(false);
				break;

			case GameState.Level2:
				Level1.SetActive(false);
				Level2.SetActive(true);
				MenuPause.SetActive(false);
				Menu.SetActive(false);
				break;

			case GameState.MenuPause:
				Level1.SetActive(false);
				Level2.SetActive(false);
				MenuPause.SetActive(true);
				Menu.SetActive(false);
				break;
		}
		CurrState = gameState;
	}
}
