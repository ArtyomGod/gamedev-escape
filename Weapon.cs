﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
	public GameObject bullet;
	public float fireRate = 0.5F;
	private float nextFire = 0.0F;
	public Side SideToShoot = Side.Right;
	public bool DoShoot { get; set; }
	
	void Update ()
	{
		if((Input.GetKey(KeyCode.E) || DoShoot) && Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;
			Shoot();
		}
	}

	private void Shoot()
	{	
		var createdBullet = Instantiate(bullet, new Vector2(this.transform.position.x + ((int)SideToShoot * 0.6f), transform.position.y), Quaternion.identity);
		createdBullet.SetActive(true);
		createdBullet.GetComponent<Bullet>().direction *= (int) SideToShoot;
	}
}
