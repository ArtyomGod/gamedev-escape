﻿using System.Collections.Generic;
using UnityEngine;

public class Level1Controller : MonoBehaviour
{
	public GameObject Player;
	public GameObject Box;
	public GameObject BlueAlien;
	public GameObject GreenAlien;
	public GameObject TimeManagerObject;
	public GameObject Boss;

	public GameObject[] GameObjects;
	private List<Vector2> GameObjectsPos;

	private Vector2 PlayerPos;
	private Vector2 BoxPos;
	private Vector2 BlueAlienPos;
	private Vector2 GreenAlienPos;
	private TimeManager GetTimeManager;


	private Time Time;

	void Start ()
	{
		GameObjectsPos = new List<Vector2>();
		GetTimeManager = TimeManagerObject.GetComponent<TimeManager>();
		IntitGameObjectsPos();
	}
	
	public void Reset()
	{
		ResetGameObjectsPos();
		ScoreManager.Reset();
		ResetPlayer();
		ResetBlueAlien();
		ResetGreenAlien();
		ResetWeaponBag();
		ResetTeleport();
		ResetTimeManager();
		ResetBoss();
	}
	
	private void SetObjectToPos(GameObject gameObject, Vector2 pos)
	{
		gameObject.transform.position = pos;
		gameObject.SetActive(true);
	}

	public void ResetWeaponBag()
	{
		GameObject.Find("PartController").GetComponent<PartOfWeaponConroller>().Reset();
	}
	public void ResetBoss()
	{
		Boss.GetComponent<Boss>().Reset();
	}
	public void ResetPlayer()
	{
		Player.GetComponent<PlayerController>().Reset();
	}

	public void ResetBlueAlien()
	{
		BlueAlien.GetComponent<EnemyBlue>().Reset();
	}

	public void ResetGreenAlien()
	{
		GreenAlien.GetComponent<GreenAlien>().Reset();
	}

	public void ResetTeleport()
	{
		GameObject.Find("Teleport").GetComponent<Teleport>().Reset();
	}

	public void ResetTimeManager()
	{
		GetTimeManager.Reset();
	}

	private void IntitGameObjectsPos()
	{
		foreach(var gameObj in GameObjects)
		{
			GameObjectsPos.Add(gameObj.transform.position);
		}
	}

	private void ResetGameObjectsPos()
	{
		int i = 0;
		foreach (var gameObj in GameObjects)
		{
			SetObjectToPos(gameObj, GameObjectsPos[i]);
			i++;
		}
	}
}
