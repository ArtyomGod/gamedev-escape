﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour {

	public GameObject bullet;
	public float fireRate = 0.5F;
	private float nextFire = 1F;
	public Side SideToShoot = Side.Left;
	public bool CharacterDetected = false;

	void Update()
	{
		if (Time.time > nextFire && CharacterDetected)
		{
			nextFire = Time.time + fireRate;
			Shoot();
			CharacterDetected = false;
		}
	}
	
	private void Shoot()
	{
		var createdBullet = Instantiate(bullet, new Vector2(this.transform.position.x + ((int)SideToShoot * 0.6f), transform.position.y), Quaternion.identity);
		createdBullet.SetActive(true);
		createdBullet.GetComponent<Bullet>().direction *= (int)SideToShoot;
	}
}
