﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class TimeManager : MonoBehaviour {


	public float defaultTimer;
	private float timer;
	Text text;
	public bool stop = false;

	void Awake()
	{
		text = GetComponent<Text>();
		text.color = new Color(255, 255, 255, 255);
		timer = defaultTimer;
	}


	void Update()
	{
		if(stop)
		{
			return;
		}
		timer -= Time.deltaTime;
		text.text = timer.ToString("0.00");
		if (timer < 10)
		{
			text.color = new Color(255, 0, 0, 255);
		}
		if (timer < 0)
		{
			GameObject.Find("LevelControll").GetComponent<Level1Controller>().Reset();
			Reset();
		}
	}

	public void Reset()
	{
		timer = defaultTimer;
		text.color = new Color(255, 255, 255, 255);
	}
}
