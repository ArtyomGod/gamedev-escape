﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCharacter : MonoBehaviour
{
	public GameObject Weapon;
	public bool IsBoss = false;
	void OnTriggerStay2D(Collider2D col)
	{
		Debug.Log("collision name = " + col.gameObject.name);
		if(col.tag == "Player")
		{
			if (IsBoss)
			{
				Weapon.GetComponent<EnemyBossWeapon>().CharacterDetected = true;
			}
			else
			{
				Weapon.GetComponent<EnemyWeapon>().CharacterDetected = true;
			}
		}
	}
}
