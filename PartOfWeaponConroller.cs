﻿using UnityEngine;

public class PartOfWeaponConroller : MonoBehaviour
{
	public GameObject[] Parts;
	public GameObject[] MiniParts;
	public int PartsActivated = 0;

	void Update ()
	{
		int i = 0;
		foreach(var part in Parts)
		{
			if(part.GetComponent<PartOfWeapon>().IsCollideWithPlayer)
			{
				ScoreManager.score += 10;
				MiniParts[i].SetActive(true);
				Parts[i].SetActive(false);

				GameObject.Find("Player").GetComponent<PlayerController>().PartsOfGunCollected++;
				part.GetComponent<PartOfWeapon>().IsCollideWithPlayer = false;
				PartsActivated++;
			}
			i++;
		}

		if(PartsActivated == MiniParts.Length)
		{
			GameObject.Find("Player").GetComponent<PlayerController>().HasSuperGun = true;
			PartsActivated = 0;
		}
	}

	public void Reset()
	{
		PartsActivated = 0;
		int i = 0;
		foreach (var part in Parts)
		{
			part.GetComponent<PartOfWeapon>().IsCollideWithPlayer = false;			
			MiniParts[i].SetActive(false);
			Parts[i].SetActive(true);		
			i++;
		}
	}
}
