﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
	public Button StartButton;
	public Button ExitButton;
	protected GlobalControll gc;
	public GameObject globalControll;
	public GameObject TimeManager;

	void Start ()
	{
		gc = globalControll.GetComponent<GlobalControll>();
	}

	public void StartGame()
	{
		TimeManager.GetComponent<TimeManager>().stop = false;
		gc.ChangeGameState(GameState.Level1);
	}

	public  void ExitGame()
	{
		Application.Quit();
	}
	public void MenuPauseOn()
	{
		TimeManager.GetComponent<TimeManager>().stop = true;
		gc.ChangeGameState(GameState.MenuPause);
	}

	public void RestartLevel()
	{
		TimeManager.GetComponent<TimeManager>().stop = false;
		gc.ChangeGameState(gc.LastState);
	}
}
