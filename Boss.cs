﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour {

	public int livesAmmount = 5;
	private Animator Anim;
	private PlayerController playerController;
	void Start()
	{
		playerController = GameObject.Find("Player").GetComponent<PlayerController>();
		Anim = GetComponent<Animator>();
	}
	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Bullet" && playerController.ReadyToBoss)
		{
			ScoreManager.score += 70;
			livesAmmount--;
		}
	}

	void Update()
	{
		if (livesAmmount <= 0)
		{
			Anim.SetBool("Death", true);
			if (Anim.GetCurrentAnimatorStateInfo(0).IsName("Death"))
			{
				ScoreManager.score += 500;
				gameObject.SetActive(false);
			}
		}
	}

	public void Reset()
	{
		livesAmmount = 5;
		Anim.SetBool("Death", false);
	}
}
