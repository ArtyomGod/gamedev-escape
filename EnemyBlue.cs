﻿using UnityEngine;

public class EnemyBlue : MonoBehaviour {

	public int livesAmmount = 3;
	private Animator Anim;
	void Start()
	{
		Anim = GetComponent<Animator>();
	}
	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.tag == "Bullet")
		{
			ScoreManager.score += 20;
			livesAmmount--;
		}
	}

	void Update()
	{
		if(livesAmmount <= 0)
		{
			Anim.SetBool("Death", true);
			if (Anim.GetCurrentAnimatorStateInfo(0).IsName("Death"))
			{
				ScoreManager.score += 100;
				gameObject.SetActive(false);
			}
		}
	}

	public void Reset()
	{
		livesAmmount = 3;
		Anim.SetBool("Death", false);
	}
}
