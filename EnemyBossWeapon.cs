﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBossWeapon : MonoBehaviour {

	public GameObject bullet;
	public float fireRate = 0.5F;
	private float nextFire = 1F;
	public Side SideToShoot = Side.Left;
	public bool CharacterDetected = false;
	public GameObject AimCollider;

	void Update()
	{
		if (Time.time > nextFire && CharacterDetected)
		{
			nextFire = Time.time + fireRate;
			Shoot();
			CharacterDetected = false;
		}
	}

	private void Shoot()
	{
		ShootUpperBullet();
		ShootMiddleBullet();
		ShootDownBullet();
	}


	private void ShootMiddleBullet()
	{
		var createdBullet = Instantiate(bullet, new Vector2(this.transform.position.x + ((int)SideToShoot * 1.6f), transform.position.y), Quaternion.identity);
		createdBullet.SetActive(true);
		createdBullet.GetComponent<Bullet>().direction *= (int)SideToShoot;
	}

	private void ShootUpperBullet()
	{
		var createdBullet = Instantiate(bullet, new Vector2(this.transform.position.x + ((int)SideToShoot * 1.6f), transform.position.y + 0.1f), Quaternion.identity);
		createdBullet.transform.rotation = Quaternion.Euler(0, 0, 45);
		createdBullet.SetActive(true);
		createdBullet.GetComponent<Bullet>().direction *= (int)SideToShoot;
		createdBullet.GetComponent<Bullet>().angle = 0.0145f;
	}
	private void ShootDownBullet()
	{
		var createdBullet = Instantiate(bullet, new Vector2(this.transform.position.x + ((int)SideToShoot * 1.6f), transform.position.y + 0.1f), Quaternion.identity);
		createdBullet.transform.rotation = Quaternion.Euler(0, 0, 45);
		createdBullet.SetActive(true);
		createdBullet.GetComponent<Bullet>().direction *= (int)SideToShoot;
		createdBullet.GetComponent<Bullet>().angle = -0.0145f;
	}
}
