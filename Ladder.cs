﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
	public float speed = 5f;
	public bool GoUp { get; set; }
	public bool GoDown { get; set; }

	void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "Player" && (Input.GetKey(KeyCode.W) || GoUp))
		{
			col.GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
		} else if (col.tag == "Player" && (Input.GetKey(KeyCode.S) || GoDown))
		{
			col.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -speed);
		}
		else if (col.tag == "Player")
		{
			col.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
		}
	}
}
