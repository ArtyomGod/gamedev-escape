﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public float direction = 0.1f;
	public Sprite LeftToRightBoom;
	public Sprite RightToLeftBoom;
	public float angle = 0f;

	void Update ()
	{
		transform.position = new Vector2(transform.position.x + direction, transform.position.y + angle);		
	}
	
	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.tag == "Bullet")
		{
			return;
		}

		if(direction > 0)
		{
			GetComponent<SpriteRenderer>().sprite = LeftToRightBoom;
		}
		else
		{
			GetComponent<SpriteRenderer>().sprite = RightToLeftBoom;
		}
		StartCoroutine(SetCountText());
		Destroy(this.gameObject);
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "BorderOfEdge" && col.gameObject.layer == 12)
		{
			Destroy(this.gameObject);
		}		
	}
	IEnumerator SetCountText()
	{
		yield return new WaitForSeconds(0.1f);			
	}
}
