﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Teleport : MonoBehaviour
{
	private Animator Anim;
	public GameState TeleportateTo = GameState.Level2;
	void Start ()
	{
		Anim = GetComponent<Animator>();
	}
	void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			if(col.gameObject.GetComponent<PlayerController>().PartsOfGunCollected != 3)
			{
				return;
			}
			Anim.SetBool("Teleportate", true);
			transform.position = new Vector2(transform.position.x, transform.position.y + 0.03f);
			if (Anim.GetCurrentAnimatorStateInfo(0).IsName("Teleportate"))
			{
				col.gameObject.SetActive(false);
				GameObject.Find("LevelControll").GetComponent<Level1Controller>().Reset();
				GameObject.Find("GlobalController").GetComponent<GlobalControll>().ChangeGameState(TeleportateTo);
				transform.position = new Vector2(transform.position.x, transform.position.y - 0.04f);
			}
		}		
	}

	public void Reset()
	{
		Anim.SetBool("Teleportate", false);
	}
}
