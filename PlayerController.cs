﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum Side
{
	Right = 1,
	Neutral = 0,
	Left = -1
}
public class PlayerController : MonoBehaviour
{
	public int Lives = 3;
	public float speed = 5f;
	public float jumpSpeed = 8f;
	public int PartsOfGunCollected = 0;
	public GameObject[] Hearts;	
	private float movement = 0f;
	private Rigidbody2D rigidBody;
	bool Grounded;
	private Vector3 _origPos;
	private Side FaceTo = Side.Right;
	Animator AnimCharacter;
	GameObject Character;
	SpriteRenderer CharacterSpriteRenderer;
	private Level1Controller level1Controller;
	public bool HasSuperGun = false;
	public bool ReadyToBoss = false;
	private Vector3 BagOriginPos;
	private Quaternion BagOriginRotate;
	public bool IsAndroidControll = true;
	private GameObject Bag;
	public bool DoJump { get; set; }
	void OnCollisionStay2D(Collision2D collider)
	{
		CheckIfGrounded();
	}

	void OnCollisionExit2D(Collision2D collider)
	{
		Grounded = false;
	}

	private void CheckIfGrounded()
	{
		RaycastHit2D[] hits;
		
		Vector2 positionToCheck = transform.position;
		hits = Physics2D.RaycastAll(positionToCheck, new Vector2(0, -1), 0.01f);
		
		if (hits.Length > 0)
		{
			Grounded = true;
		}
	}

	void Start()
	{
		Character = GameObject.Find("Character");
		Bag = GameObject.Find("Bag");
		BagOriginPos = Bag.transform.position;
		BagOriginRotate = Bag.transform.rotation;

		AnimCharacter = Character.GetComponent<Animator>();
	
		CharacterSpriteRenderer = Character.GetComponent<SpriteRenderer>();
	
		_origPos = transform.position;
		rigidBody = GetComponent<Rigidbody2D>();
		level1Controller = GameObject.Find("LevelControll").GetComponent<Level1Controller>();
		
	}

	public void MoveHorizontal(int currMovement)
	{
		movement = currMovement;
	}
	public void StopMoveHorizontal()
	{
		movement = 0;
	}
	void Update()
	{
		Side newFaceTo = Side.Neutral;
		if(!IsAndroidControll)
		{
			movement = Input.GetAxis("Horizontal");
		}
		if (movement > 0)
		{
			AnimCharacter.SetBool("Run", true);
			AnimCharacter.SetBool("Idle", false);
			newFaceTo = Side.Right;
			rigidBody.velocity = new Vector2(movement * speed, rigidBody.velocity.y);
		}
		else if (movement < 0)
		{
			AnimCharacter.SetBool("Run", true);
			AnimCharacter.SetBool("Idle", false);
			newFaceTo = Side.Left;
			rigidBody.velocity = new Vector2(movement * speed, rigidBody.velocity.y);
		}
		else
		{
			AnimCharacter.SetBool("Run", false);
			AnimCharacter.SetBool("Idle", true);
			rigidBody.velocity = new Vector2(0, rigidBody.velocity.y);
		}

		FlipCharacter(newFaceTo);

		if ((Input.GetButtonDown("Jump") || DoJump) && Grounded)
		{
			rigidBody.velocity = new Vector2(rigidBody.velocity.x, jumpSpeed);
		}
		if(Lives <= 0)
		{
			if (AnimCharacter.GetCurrentAnimatorStateInfo(0).IsName("Death"))
			{
				Kill();
			}
		}

		if(HasSuperGun)
		{
			ReadyToBoss = true;
			FlipGun(true);
			HasSuperGun = false;
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Bullet" || col.gameObject.tag == "GreenAlien")
		{
			AnimCharacter.SetBool("Run", false);
			AnimCharacter.SetBool("Idle", false);
			AnimCharacter.SetBool("Hurt", true);

			Debug.Log(Lives);

			DecrementLives();

			if (Lives <= 0)
			{
				AnimCharacter.SetBool("Run", false);
				AnimCharacter.SetBool("Idle", false);
				AnimCharacter.SetBool("Hurt", false);
				AnimCharacter.SetBool("Death", true);
				if (AnimCharacter.GetCurrentAnimatorStateInfo(0).IsName("Death"))
				{
					Kill();
				}
			}
		}

		AnimCharacter.SetBool("Hurt", false);
	}

	private void FlipCharacter(Side newValue)
	{
		if (newValue != FaceTo && newValue != Side.Neutral)
		{
			FaceTo = newValue;

			gameObject.transform.Find("Weapon").GetComponent<Weapon>().SideToShoot = FaceTo;	
			transform.localScale =  new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		}

	}
	
	private void ChangeCollorOfCharacter()
	{
		switch (Lives)
		{
			case 3:
				CharacterSpriteRenderer.color = new Color(255, 255, 255, 255);
				break;
			case 2:
				CharacterSpriteRenderer.color = new Color(255, 136, 165, 255);
				break;
			case 1:
				CharacterSpriteRenderer.color = new Color(255, 69, 114, 255);
				break;
			case 0:
				CharacterSpriteRenderer.color = new Color(255, 0, 62, 255);
				break;			
		}
	}

	private void DisableHearts()
	{
		switch (Lives)
		{
			case 3:
				break;
			case 2:
				Hearts[2].SetActive(false);
				break;
			case 1:
				Hearts[1].SetActive(false);
				break;
			case 0:
				Hearts[0].SetActive(false);
				break;
		}
	}

	private void FlipGun(bool placeHorizontal)
	{
		if(placeHorizontal)
		{
			Bag.transform.rotation = Quaternion.Euler(0, 0, -90);
			Bag.transform.position = new Vector3(Bag.transform.position.x, -0.15f, 0f);
		}
		else
		{
			Bag.transform.rotation = BagOriginRotate;
			Bag.transform.position = BagOriginPos;
		}
	}

	private void Kill()
	{
		gameObject.SetActive(false);
		level1Controller.Reset();
	}

	public void DecrementLives()
	{
		Lives--;
		ChangeCollorOfCharacter();
		DisableHearts();
	}

	public void Reset()
	{
		ReadyToBoss = false;
		HasSuperGun = false;
		FlipGun(false);
		Lives = 3;
		PartsOfGunCollected = 0;
		ResetHearts();
		ChangeCollorOfCharacter();
		ResetAnimation();
	}

	public void ResetHearts()
	{
		foreach(var heart in Hearts)
		{
			heart.SetActive(true);
		}
	}

	public void ResetAnimation()
	{
		AnimCharacter.SetBool("Run", false);
		AnimCharacter.SetBool("Idle", false);
		AnimCharacter.SetBool("Hurt", false);
		AnimCharacter.SetBool("Death", false);
	}
}