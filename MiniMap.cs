﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MiniMap : MonoBehaviour {

	private Button button;
	public Camera camera;
	private bool isClicked = false;
	private float originCameraSize;
	private Vector2 originCameraPos;
	private Vector2 setCameraPos;
	private CameraController cameraController;	
	void Start()
	{
		button = GetComponent<Button>();
		cameraController = camera.GetComponent<CameraController>();
		originCameraSize = camera.orthographicSize;
		originCameraPos = camera.gameObject.transform.position;
		setCameraPos = new Vector2(originCameraPos.x - 4, setCameraPos.y);
	}

	public void Postpone()
	{
		if(!isClicked)
		{
			cameraController.offset = new Vector3(5, cameraController.offset.y, cameraController.offset.z);
			camera.orthographicSize = 5;
		}
		else
		{
			cameraController.offset = new Vector3(0, cameraController.offset.y, cameraController.offset.z);
			camera.orthographicSize = originCameraSize;
		}
		isClicked = !isClicked;
	}
}
