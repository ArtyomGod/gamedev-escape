﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPause : MenuController
{	
	void Start ()
	{
		gc = globalControll.GetComponent<GlobalControll>();
	}
	
	void Update () {
		
	}
	public void MenuPauseOn()
	{
		gc.ChangeGameState(GameState.MenuPause);
	}

	public void RestartLevel()
	{
		gc.ChangeGameState(gc.LastState);
	}
}
