﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartOfWeapon : MonoBehaviour
{
	public bool IsCollideWithPlayer = false;
	void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			IsCollideWithPlayer = true;
		}
	}
}
