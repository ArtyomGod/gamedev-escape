﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenAlien : MonoBehaviour
{
	public Side FaceTo = Side.Right;
	public int Lives = 1;
	private Rigidbody2D rigidBody;
	public float speed = 0.5f;
	private Animator Anim;
	
	void Start ()
	{
		rigidBody = GetComponent<Rigidbody2D>();
		Anim = GetComponent<Animator>();
	}
	
	void Update ()
	{
		rigidBody.velocity = new Vector2((int)FaceTo * speed, rigidBody.velocity.y);
		if(Lives <= 0)
		{
			if (Anim.GetCurrentAnimatorStateInfo(0).IsName("Death"))
			{
				Kill();
			}
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		Debug.Log("GreenAlien");
		if(col.gameObject.tag == "Box")
		{
			if (col.contacts.Length > 0)
			{
				ContactPoint2D contact = col.contacts[0];
				if (Vector3.Dot(contact.normal, Vector3.down) > 0.5)
				{
					ScoreManager.score += 50;
					Lives--;
					Anim.SetBool("Death", true);
					/*if (Anim.GetCurrentAnimatorStateInfo(0).IsName("Death"))
					{
						Kill();
					}*/
				}
			}
		}
		if (col.gameObject.tag == "Bullet")
		{
			ScoreManager.score += 20;
			Anim.SetBool("Death", true);
			Lives--;
		}
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "BorderOfEdge")
		{
			FaceTo = FaceTo == Side.Right ? Side.Left : Side.Right;

			transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
		}
	}	

	void Kill()
	{
		ScoreManager.score += 100;
		gameObject.SetActive(false);		
	}

	public void Reset()
	{
		Anim.SetBool("Death", false);
		Lives = 1;
	}
}
